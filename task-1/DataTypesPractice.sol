// SPDX-License-Identifier: MIT

pragma solidity 0.8.13;

interface IDataTypesPractice {
    function getInt256() external view returns(int256);
    function getUint256() external view returns(uint256);
    function getIint8() external view returns(int8);
    function getUint8() external view returns(uint8);
    function getBool() external view returns(bool);
    function getAddress() external view returns(address);
    function getBytes32() external view returns(bytes32);
    function getArrayUint5() external view returns(uint256[5] memory);
    function getArrayUint() external view returns(uint256[] memory);
    function getString() external view returns(string memory);

    function getBigUint() external pure returns(uint256);
}

contract DataTypesPractice is IDataTypesPractice {
    int256 i256 = 1;
    uint256 ui256 = 1;
    int8 i8 = 1;
    uint8 ui8 = 1;
    bool b = true;
    address a = address(this);
    bytes32 b32 = "bytes32";
    uint256[5] ui256s5 = [1, 2, 3, 4, 5];
    uint256[] ui256s = [1, 2, 3, 4, 5];
    string s = "Hello World!";

    function getInt256() external view returns(int256) {
        return i256;
    }
    function getUint256() external view returns(uint256) {
        return ui256;
    }
    function getIint8() external view returns(int8) {
        return i8;
    }
    function getUint8() external view returns(uint8) {
        return ui8;
    }
    function getBool() external view returns(bool) {
        return b;
    }
    function getAddress() external view returns(address) {
        return a;
    }
    function getBytes32() external view returns(bytes32) {
        return b32;
    }
    function getArrayUint5() external view returns(uint256[5] memory) {
        return ui256s5;
    }
    function getArrayUint() external view returns(uint256[] memory) {
        return ui256s;
    }
    function getString() external view returns(string memory) {
        return s;
    }
    function getBigUint() external pure returns(uint256) {
        uint v1 = 1;
        uint v2 = 2;
        unchecked {
            return v1-v2;
        }
    }
}
