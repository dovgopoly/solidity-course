// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./First.sol";

interface ISecond {
    function withdrawSafe(address payable holder) external;
    function withdrawUnsafe(address payable holder) external;
}

contract Second is First, ISecond, ReentrancyGuard {
    mapping(address => uint256) public balance;

    receive() external payable {
        balance[tx.origin] += msg.value;
    }

    fallback() external payable {
        balance[msg.sender] += msg.value;
    }

    function sum() external view override returns (uint256) {
        return ePublic + eInternal;
    }

    function withdrawSafe(address payable holder) external nonReentrant {
        _withdraw(holder);
    }

    function withdrawUnsafe(address payable holder) external {
        _withdraw(holder);
    }

    function _withdraw(address holder) private {
        (bool success,) = holder.call{value: balance[holder]}("");
        require(success, "Failed to withdraw");
        balance[holder] = 0;
    }
}