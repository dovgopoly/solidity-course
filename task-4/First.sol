// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IFirst {
    function setPublic(uint256 num) external;
    function setPrivate(uint256 num) external;
    function setInternal(uint256 num) external;
    function sum() external view returns (uint256);
    function sumFromSecond(address contractAddress) external returns (uint256);
    function callExternalReceive(address payable contractAddress) external payable;
    function callExternalFallback(address payable contractAddress) external payable;
    function getSelector() external pure returns (bytes memory);
}

contract First is IFirst, Ownable {
    uint256 public ePublic;
    uint256 private ePrivate;
    uint256 internal eInternal;

    modifier ifExpectedMsgValue(uint256 expectedMsgValue) {
        require(
            msg.value == expectedMsgValue,
            "Unexpected amount of ether has been passed"
        );
        _;
    }

    function setPublic(uint256 num) external onlyOwner {
        ePublic = num;
    }

    function setPrivate(uint256 num) external onlyOwner {
        ePrivate = num;
    }

    function setInternal(uint256 num) external onlyOwner {
        eInternal = num;
    }

    function sum() external view virtual returns (uint256) {
        return ePublic + eInternal + ePrivate;
    }

    function sumFromSecond(address contractAddress) external view returns (uint256) {
        (bool success, bytes memory data) = contractAddress.staticcall(
            abi.encodeWithSignature("sum()")
        );
        require(success, "Failed to call sum()");
        return abi.decode(data, (uint256));
    }

    function callExternalReceive(
        address payable contractAddress
    )
        external
        payable
        ifExpectedMsgValue(0.0001 ether)
    {
        (bool success,) = contractAddress.call{value: msg.value}("");
        require(success, "Failed to call receive()");
    }

    function callExternalFallback(
        address payable contractAddress
    )
        external
        payable
        ifExpectedMsgValue(0.0002 ether)
    {
        (bool success,) = contractAddress.call{value: msg.value}(
            abi.encodeWithSignature("nonExistingFunction()")
        );
        require(success, "Failed to call fallback()");
    }

    function getSelector() external pure returns (bytes memory) {
        return abi.encodePacked(
            this.ePublic.selector,
            this.setPublic.selector,
            this.setPrivate.selector,
            this.setInternal.selector,
            this.sum.selector,
            this.sumFromSecond.selector,
            this.callExternalReceive.selector,
            this.callExternalFallback.selector,
            this.getSelector.selector
        );
    }
}