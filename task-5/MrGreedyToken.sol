// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.9;

import "./SimpleToken.sol";

interface IMrGreedyToken {
    function treasury() external view returns (address);

    function getResultingTransferAmount(uint256 amount_) external view returns (uint256);
}

contract MrGreedyToken is SimpleToken, IMrGreedyToken {
    address public immutable treasury;

    constructor(address _treasury) {
        treasury = _treasury;
    }

    function name() public view virtual override returns (string memory) {
        return "MrGreedyToken";
    }

    function symbol() public view virtual override returns (string memory) {
        return "MRG";
    }

    function decimals() public view virtual override returns (uint8) {
        return 6;
    }

    function getResultingTransferAmount(uint256 _amount) public view returns (uint256) {
        uint256 tenTokens = 10 * 10 ** decimals();
        return _amount < tenTokens ? 0 : _amount - tenTokens;
    }

    function _transfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal override {
        uint256 resultingAmount = getResultingTransferAmount(_amount);
        super._transfer(_from, treasury, _amount - resultingAmount);
        super._transfer(_from, _to, resultingAmount);
    }
}
