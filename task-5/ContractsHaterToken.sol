// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/utils/Address.sol";
import "./SimpleToken.sol";

interface IContractsHaterToken {
    function addToWhitelist(address candidate_) external;

    function removeFromWhitelist(address candidate_) external;
}

contract ContractsHaterToken is SimpleToken, IContractsHaterToken {
    using Address for address;

    mapping(address => bool) public whiteList;

    function name() public view virtual override returns (string memory) {
        return "ContractsHaterToken";
    }

    function symbol() public view virtual override returns (string memory) {
        return "CHT";
    }

    function addToWhitelist(address _candidate) external onlyOwner {
        require(
            _candidate.isContract(),
            "Ordinary addresses should not be added to whitelist"
        );
        require(
            !whiteList[_candidate], "Contract is already in whitelist"
        );
        whiteList[_candidate] = true;
    }

    function removeFromWhitelist(address _candidate) external onlyOwner {
        require(whiteList[_candidate], "Contract is absent in whitelist");

        whiteList[_candidate] = false;
    }

    function _beforeTokenTransfer(
        address _from,
        address _to,
        uint256
    )
        internal
        view
        override
    {
        // It is possible to make mint & burn operations
        if (_from == address(0) || _to == address(0)) {
            return;
        }

        if (!_to.isContract()) {
            return;
        }

        require(
            whiteList[_to],
            "Cannot transfer token to the contract which is absent in whitelist"
        );
    }
}