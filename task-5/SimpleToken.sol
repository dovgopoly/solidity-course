// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./SimpleToken.sol";

interface ISimpleToken {
    function mint(address to_, uint256 amount_) external;

    function burn(uint256 amount_) external;
}

contract SimpleToken is ERC20, Ownable {
    constructor() ERC20("", "") {}

    function name() public view virtual override returns (string memory) {
        return "SimpleToken";
    }

    function symbol() public view virtual override returns (string memory) {
        return "ST";
    }

    function mint(address _to, uint256 _amount) external onlyOwner {
        _mint(_to, _amount);
    }

    function burn(uint256 _amount) external {
        _burn(msg.sender, _amount);
    }
}
